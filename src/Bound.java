//Pong - John Salis

import java.awt.*;
import java.util.*;

public class Bound extends Rectangle
{
	
	public Bound(int x, int y, int width, int height)
	{
		super(x, y, width, height);
	}
	
	
	public void draw(Graphics2D g)
	{
		g.setColor(Color.BLACK);
		g.fillRect((int)getX(), (int)getY(), getSize().width, getSize().height);
	}
}
