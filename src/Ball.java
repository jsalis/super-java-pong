//Pong - John Salis

import java.awt.*;
import java.util.*;

public class Ball extends Rectangle implements Cloneable
{
	
	private final int TRAIL_LENGTH = 51;
	private LinkedList<Point> trail = new LinkedList<Point>();
	private boolean isTrailing = false;
	
	private int velocity = 8;
	private double locationX, locationY;
	private double velocityX, velocityY;
	
	private Vector<Rectangle> collisions = new Vector<Rectangle>(6);
	private Vector<Ball> bawls;
	private Random random = new Random();
	
	private Player lastPlayer;											// last player to hit the ball
	
	public Ball(int x, int y, int size)
	{
		super(x, y, size, size);
		locationX = x;
		locationY = y;
		
		setVelocityX(((double)random.nextInt((velocity*10)-25) + 25) / 10, random.nextBoolean());
		if (random.nextBoolean()) velocityX = -velocityX;
	}
	
	
	
	public void setVelocityX(double vX, boolean isPositiveY)
	{
		if (vX <= velocity && vX >= -velocity)
		{
			velocityX = vX;
			velocityY = Math.sqrt((Math.pow(velocity, 2)) - (Math.pow(velocityX, 2)));
			if (!isPositiveY) velocityY = -velocityY;
		}
	}
	
	
	
	public void setVelocityY(double vY, boolean isPositiveX)
	{
		if (vY <= velocity && vY >= -velocity)
		{
			velocityY = vY;
			velocityX = Math.sqrt((Math.pow(velocity, 2)) - (Math.pow(velocityY, 2)));
			if (!isPositiveX) velocityX = -velocityX;
		}
	}
	
	
	
	public double getVelocityX()
	{
		return velocityX;
	}
	
	
	
	public double getVelocityY()
	{
		return velocityY;
	}
	
	
	
	public int getVelocity()
	{
		return velocity;
	}
	
	
	
	public void setVelocity(int v)
	{
		double angle = Math.asin(velocityX / velocity);
		velocityX = Math.sin(angle) * v;
		velocityY = Math.cos(angle) * v;
		velocity = v;
	}
	
	
	
	public void move()
	{
		updateTrail();
		
		locationX += velocityX;
		locationY += velocityY;
		this.setLocation((int) locationX, (int) locationY);
		
		for (int i = 0; i < collisions.size(); i++)
		{
			testCollision(collisions.get(i));
		}
		
		if (bawls != null)
		{
			for (int i = 0; i < bawls.size(); i++)
			{
				if (!bawls.get(i).equals(this)) testCollision(bawls.get(i));
			}
		}
	}
	
	
	
	private void updateTrail()
	{
		if (isTrailing)
		{
			trail.add(new Point(x, y));
			if (trail.size() > TRAIL_LENGTH)
			{
				trail.remove();
			}
		} 
		else if (trail.size() != 0)
		{
			trail.clear();
		}
	}
	
	
	
	public void testCollision(Rectangle rect)
	{
		if (this.intersects(rect))
		{
			Rectangle unionRect = this.intersection(rect);
			if (unionRect.getWidth() < unionRect.getHeight())
			{
				if (locationX < rect.getX())
				{
					locationX -= unionRect.getWidth();
				}
				else
				{
					locationX += unionRect.getWidth();
				}
				velocityX = -velocityX;
			}
			else if (unionRect.getWidth() > unionRect.getHeight())
			{
				if (locationY < rect.getY())
				{
					locationY -= unionRect.getHeight();
				}
				else
				{
					locationY += unionRect.getHeight();
				}
				velocityY = -velocityY;
			}
			else
			{
				velocityY = -velocityY;
				velocityX = -velocityX;
			}
			this.setLocation((int) locationX, (int) locationY);
		}
	}
	
	
	
	public void addCollision(Rectangle rect)
	{
		collisions.add(rect);
	}
	
	
	
	public void setBallCollision(Vector<Ball> balls)
	{
		bawls = balls;
	}
	
	
	
	public void draw(Graphics2D g)
	{
		g.setColor(Color.BLACK);
		g.fillOval(x, y, width, height);
		if (isTrailing)
		{
			int alpha = 0;
			for (int i = 0; i < trail.size(); i++)
			{
				g.setColor(new Color(255, 255, 0, alpha));
				g.fillOval(trail.get(i).x, trail.get(i).y, width, height);
				alpha += 255 / TRAIL_LENGTH;
			}
		}
	}
	
	
	
	public void setTrailing(boolean t)
	{
		isTrailing = t;
	}
	
	
	
	public boolean isTrailing()
	{
		return isTrailing;
	}
	
	
	
	public void setLastPlayer(Player player)
	{
		lastPlayer = player;
	}
	
	
	
	public Player getLastPlayer()
	{
		return lastPlayer;
	}
	
	
	
	public void setSize(int size)
	{
		this.setSize(size, size);
	}
	
	
	
    public Object clone()
    {
    	return super.clone();
    }
	
}
