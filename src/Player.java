//Pong - John Salis

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.Vector;

public class Player
{

	private Direction dir;
	private Paddle pad;
	private int score;
	private Rectangle scoreZone;
	private PowerUp powerUp;
	
	public Player(int paddleX, int paddleY, int paddleH)
	{
		pad = new Paddle(this, paddleX, paddleY, paddleH);
		dir = Direction.IDLE;
		resetScore();
	}
	
	
	public Paddle getPaddle()
	{
		return pad;
	}
	
	
	public void setDirection(Direction d)
	{
		dir = d;
	}
	
	
	public Direction getDirection()
	{
		return dir;
	}
	
	
	public void movePaddle()
	{
		pad.move(dir);
	}
	
	
	public void addCollision(Rectangle rect)
	{
		pad.addCollision(rect);
	}
	
	
	public void setBallCollision(Vector<Ball> balls)
	{
		pad.setBallCollision(balls);
	}
	
	
	public void draw(Graphics2D g)
	{
		pad.draw(g);
	}
	
	
	public void addScore(int s)
	{
		score += s;
	}
	
	
	public int getScore()
	{
		return score;
	}
	
	
	public void resetScore()
	{
		score = 0;
	}
	
	
	public void setPowerUp(PowerUp pU)
	{
		removePowerUp();
		powerUp = pU;
		switch (powerUp.getType())
		{
			case SUPER_SHOT:
				pad.setCharged(true);
				break;
				
			case MEGA_PADDLE:
				pad.setTargetHeight((int)(pad.getTargetHeight() * 1.5));
				break;
		}
	}
	
	
	public void removePowerUp()
	{
		if (powerUp != null)
		{
			switch (powerUp.getType())
			{
				case MEGA_PADDLE:
					pad.setTargetHeight((int)(pad.getTargetHeight() / 1.5));
					break;
			}
		}
		powerUp = null;
	}
	
}
