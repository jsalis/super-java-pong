//Pong - John Salis

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.*;

public class GameFrame extends JFrame
{
	public final static String TITLE = "Super Pong!";
	PongBoard pongBoard;
	
	public GameFrame()
	{
		setLayout(new BorderLayout());
		setTitle(TITLE);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(PongBoard.WIDTH, PongBoard.HEIGHT);
		setResizable(true);
		
		pongBoard = new PongBoard();
		add(pongBoard);
		
		// makes pongBoard get the focus whenever frame is activated.
		this.addWindowFocusListener(new WindowAdapter() {
		    public void windowGainedFocus(WindowEvent e) {
		        pongBoard.requestFocusInWindow();
		    }
		});
		
		pack();
		validate();
		setLocationRelativeTo(null);
		setVisible(true);
		pongBoard.start();
	}
	
	
	
	public static void main(String[] args)
	{
		new GameFrame();
	}

}
