//Pong - John Salis

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.*;

public class Paddle extends Rectangle
{
	
	private final double MIN_POWER = 0.4;
	private double locationX, locationY;
	private double velocity = 0;
	private double friction = 0.75;
	private double acceleration = 2.75;
	private int charge = 0;
	private int targetHeight;
	private boolean isCharged = false;
	
	private Player player; 	// player that owns the paddle
	private Vector<Rectangle> collisions = new Vector<Rectangle>(2);
	private Vector<Ball> bawls;
	
	public Paddle(Player p, int x, int y, int height)
	{
		super(x, y, 12, height);
		targetHeight = height;
		locationX = x;
		locationY = y;
		player = p;
	}
	
	
	
	public void move(Direction dir)
	{
		switch (dir)
		{
			case UP:
				velocity += acceleration;
				break;
				
			case DOWN:
				velocity -= acceleration;
				break;
				
			case IDLE:
				//
				break;
		}
		velocity *= friction;
		locationY -= velocity;
		
		if (height < targetHeight)
		{
			this.setSize(width, height + 2);
			locationY -= 1;
		}
		else if (height > targetHeight)
		{
			this.setSize(width, height - 2);
			locationY += 1;
		}
		if (isCharged && charge < 150)
		{
			charge += 6;
		}
		
		this.setLocation((int) locationX, (int) locationY);
		for (int i = 0; i < collisions.size(); i++)
		{
			testCollision(collisions.get(i));
		}
		if (bawls != null)
		{
			for (int i = 0; i < bawls.size(); i++)
			{
				testBallCollision(bawls.get(i));
			}
		}
	}
	
	
	
	private void testCollision(Rectangle rect)
	{
		if (this.intersects(rect))
		{
			Rectangle unionRect = this.intersection(rect);
			if (locationY < rect.getY())
			{
				locationY -= unionRect.getHeight();
			}
			else
			{
				locationY += unionRect.getHeight();
			}
			this.setLocation((int) locationX, (int) locationY);
			velocity = 0;
		}
	}
	
	
	
	private void testBallCollision(Ball ball)
	{
		if (this.intersects(ball))
		{
			Rectangle unionRect = this.intersection(ball);
			if (unionRect.getWidth() < unionRect.getHeight())
			{
				double yDiff = ball.getCenterY() - this.getCenterY();
				boolean isPositiveY;
				if (yDiff < 0)
				{
					isPositiveY = false;
				}
				else isPositiveY = true;
				
				double power = 1 - (Math.abs(yDiff*2) / height);
				power = (1 - MIN_POWER) * power + MIN_POWER;
				
				if (locationX < ball.getX())
				{
					ball.setVelocityX(power * ball.getVelocity(), isPositiveY);
				}
				else
				{
					ball.setVelocityX(-power * ball.getVelocity(), isPositiveY);
				}
				if (isCharged)
				{
					ball.setVelocity(11);
					ball.setTrailing(true);
					player.removePowerUp();
					this.setCharged(false);
				}
				ball.setLastPlayer(player);
			}
			else ball.testCollision(this);
		}
	}
	
	
	
	public void addCollision(Rectangle rect)
	{
		collisions.add(rect);
	}
	
	
	
	public void setBallCollision(Vector<Ball> balls)
	{
		bawls = balls;
	}
	
	
	
	public void draw(Graphics2D g)
	{
		g.setColor(Color.GRAY);
		g.fillRect(x, y, width, height);
		g.setColor(new Color(175, 175, 175));
		g.fillRect(x + width - 4, y, 4, height);
		if (isCharged || charge > 0)
		{
			g.setColor(new Color(255, 255, 0, charge));
			g.setStroke(new BasicStroke(6));
			g.drawRect(x, y, width, height);
		}
	}
	
	
	
	public void setCharged(boolean c)
	{
		isCharged = c;
		if (!isCharged) charge = 0;
	}
	
	
	
	public boolean isCharged()
	{
		return isCharged;
	}
	
	
	
	public void setTargetHeight(int h)
	{
		targetHeight = h;
	}
	
	
	
	public int getTargetHeight()
	{
		return targetHeight;
	}
	
}
