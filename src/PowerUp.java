//Pong - John Salis

import java.awt.*;
import java.util.*;

public class PowerUp extends Rectangle
{
	
	public static final byte SIZE = 30;
	private boolean isAquired = false;
	private PowerUpType type;
	
	private int alpha = 255;
	private Vector<Ball> bawls;
	private Random random = new Random();
	
	public PowerUp(int x, int y)
	{
		super(x - (SIZE/2), y - (SIZE/2), SIZE, SIZE);
		int r = random.nextInt(3);
		switch (r) {
		case 0:
			type = PowerUpType.SUPER_SHOT;
			break;
		case 1:
			type = PowerUpType.MULTI_BALL;
			break;
		case 2:
			type = PowerUpType.MEGA_PADDLE;
			break;
		}
	}
	
	
	
	public PowerUp(int x, int y, PowerUpType t)
	{
		super(x, y, SIZE, SIZE);
		type = t;
	}
	
	
	
	public void setBallCollision(Vector<Ball> balls)
	{
		bawls = balls;
	}
	
	
	
	private void testBallCollision(Ball ball)
	{
		if (this.intersects(ball) && ball.getLastPlayer() != null)
		{
			if (type == PowerUpType.MULTI_BALL)
			{
				this.multiplyBall(ball);
				ball.getLastPlayer().removePowerUp();
			}
			else
			{
				ball.getLastPlayer().setPowerUp(this);
			}
			this.setAquired(true);
		}
	}
	
	
	
	private void multiplyBall(Ball ball)
	{
		Ball ballOne = (Ball)ball.clone();
		Ball ballTwo = (Ball)ball.clone();
		
		ballOne.setLastPlayer(null);
		ballTwo.setLastPlayer(null);
		
		Boolean isPositiveY;
		if (ball.getVelocityY() > 0) isPositiveY = true;
		else isPositiveY = false;
		
		ballOne.setVelocityX(ball.getVelocityX() / 2, isPositiveY);
		ballTwo.setVelocityX(ball.getVelocityX() / 4, isPositiveY);
		
		bawls.add(ballOne);
		bawls.add(ballTwo);
	}
	
	
	
	public PowerUpType getType()
	{
		return type;
	}
	
	
	
	public void setAquired(boolean aquired)
	{
		isAquired = aquired;
	}
	
	
	
	public boolean isAquired()
	{
		return isAquired;
	}
	
	
	
	public void move()
	{
		if (isAquired && alpha > 0)
		{
			this.setSize(width + 2, height + 2);
			this.setLocation(x - 1, y - 1);
			alpha -= 5;
		}
		if (!isAquired && bawls != null)
		{
			for (int i = 0; i < bawls.size(); i++)
			{
				testBallCollision(bawls.get(i));
			}
		}
	}
	
	
	
	public void draw(Graphics2D g)
	{
		switch (type)
		{
			case SUPER_SHOT:
				g.setColor(new Color(255, 255, 0, alpha));
				g.fillRect(x, y, width, height);
				break;
				
			case MEGA_PADDLE:
				g.setColor(new Color(0, 255, 255, alpha));
				g.fillRect(x, y, width, height);
				break;
				
			case MULTI_BALL:
				g.setColor(new Color(255, 0, 255, alpha));
				g.fillRect(x, y, width, height);
				break;
		}	
	}
	
}
