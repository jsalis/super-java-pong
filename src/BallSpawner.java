//Pong - John Salis

import java.awt.*;
import java.util.*;

public class BallSpawner extends Rectangle
{
	
	public static final byte SIZE = 20;
	private int locationY;
	private int amp = 100;
	private int angle1 = 0;
	private int angle2 = 0;
	
	public BallSpawner(int x, int y)
	{
		super(x - (SIZE/2), y - (SIZE/2), SIZE, SIZE);
		locationY = y;
	}
	
	
	public void move()
	{
		angle1 += 6;
		if (angle1 >= 360) angle1 = 0;
		
		angle2 += 1;
		if (angle2 >= 360) angle2 = 0;
		
		this.setLocation(x, (int) (Math.sin(Math.toRadians(angle2)) * amp) + locationY);
	}
	
	
	public void draw(Graphics2D g)
	{
		g.setColor(new Color(225, 225, 225));
		g.setStroke(new BasicStroke(4));
		g.drawArc(x, y, width, height, angle1, 90);
		g.drawArc(x, y, width, height, angle1 + 180, 90);
	}
}
